import json
import requests
import openlibrary as ol
#from openlibrary import get_book_titles, pretty_print_titles
#from closedlibrary import get_book_titles as cl_get_book_titles

topic = input("What book topic would you see? ")
url = 'https://openlibrary.org/subjects/' + topic + '.json'

try:
    book_data = ol.get_resource(url)
    book_titles = ol.get_book_titles(book_data)
    ol.pretty_print_titles(book_titles, truncate=40)
except Exception as e:
    print(e)
