import json
import requests

def get_resource(url):
    # succesfull return converted data (from JSON)
    # or raise exceptions if response status_code is not 200
    api_session = requests.session()
    # do a GET request
    print("Getting data from ", url)
    response = api_session.get(url)

    # test for success
    if response.status_code == 200:
        #print(response.text)  # json string
        data = json.loads(response.text) # convert to python dict
        print("The data is of type", type(data))
        return data
    else:
        raise Exception("Could not connect to " + url)


# filename: openlibrary.py
def get_book_titles(data):
    book_titles = []
    book_list = data["works"]

    for book in book_list:
        # book is a dictionary
        if 'title' in book:
            title = book['title']
            book_titles.append(title)

    return book_titles

def pretty_print_titles(book_titles, truncate=None):
    for title in book_titles:
        if int(truncate) > 0 and len(title) > int(truncate):
            # slice the book_titles
            title = title[:truncate] + '...'
        print('-', title)
